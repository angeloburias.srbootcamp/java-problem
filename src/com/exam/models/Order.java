package com.exam.models;

import java.io.*;
import java.util.*;

public final class Order implements Serializable {

    private final List<OrderItem> orderItems = new ArrayList<>();
    private final String filename = "fileonly";
    private final HashMap<Item, OrderItem> itemkey = new HashMap<>();
    private final List<OrderItem> itemList = new ArrayList<OrderItem>();
    public Order(OrderItem orderItems) throws IOException, ClassNotFoundException {
        this.orderItems.add(orderItems);
        if(orderItems.item.getItemType() == 1){ // 1 means item type is service
            // make item a key
            itemkey.put(orderItems.item, orderItems);
            SerializedObject();
            getItems();
        }else { // else is material, so tax is implemented
            // make item a key
            itemkey.put(orderItems.item, orderItems);
            SerializedObject();
            System.out.println("Order Total: " + getOrderTotal(orderItems.item.getPrice(), 12));
            getItems();
        }
    }

    // Returns the total order cost after the tax has been applied
    // based off on tax rate here in ph
    public float getOrderTotal(float price, float taxRate){
        return (price / taxRate);
    }
    public Collection getItems() throws IOException, ClassNotFoundException {

            Collections.shuffle(itemList);
            DeserializedObject();
            Collections.sort(orderItems, Comparator.comparing((OrderItem item) -> item.getItem().getName()));
            orderItems.stream().forEach(System.out::println);

            for (OrderItem obj : orderItems){
                System.out.println("Item name: " +obj.getItem().getName());
                System.out.println("Item price: " +obj.getItem().getPrice());
                System.out.println("Item type: " + obj.getItem().getItemType());
                itemList.add(obj);
            }
            return itemList;
    }
    private void DeserializedObject() throws IOException, ClassNotFoundException {
        // Reading the object from a file
        FileInputStream file = new FileInputStream(filename);
        ObjectInputStream in = new ObjectInputStream(file);

        // Method for deserialization of object
        Object object1 = (Object)in.readObject();

        in.close();
        file.close();
    }
    private void SerializedObject () throws IOException {
        FileOutputStream file = new FileOutputStream(filename);
        ObjectOutputStream out = new ObjectOutputStream(file);
        out.writeObject(this.orderItems.getClass());
        file.close();
    }

}
