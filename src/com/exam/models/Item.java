package com.exam.models;

public final class Item {

    private final Integer key;
    private final String name;
    private final float price;

    private final int itemType;
    public Item(Integer key, String name, float price, int type)
    {
        this.key = key;
        this.name = name;
        this.price = price;
        this.itemType = type;
    }

    public Integer getKey()
    {
        return key;
    }
    public String getName()
    {
        return name;
    }
    public float getPrice()
    {
        return price;
    }
    public int getItemType()
    {
        return itemType;
    }
}
