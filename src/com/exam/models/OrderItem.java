package com.exam.models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OrderItem{
    public Item item;
    public  int quantity;

    public OrderItem(){ }

    public OrderItem(Item item, int quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public void createOrder(List<OrderItem> oi) throws IOException, ClassNotFoundException {
        for (OrderItem orders : oi){
            var order = new Order(orders);
        }
    }

    public float getOrderTotal(float price, int taxRate) throws IOException, ClassNotFoundException {
        List<OrderItem> orders = new ArrayList<>();
        OrderItem or = new OrderItem();

        Item item1 = new Item(1, "item1", 1, 2);

        orders.add(new OrderItem( item1, 1));
        or.createOrder(orders);

        return or.getOrderTotal(price, taxRate);
    }

    public Item getItem(){
        return this.item;
    }
    public void setItem(Item item){
        this.item = item;
    }
    public void setQuantity(Integer quan){
        this.quantity = quan;
    }
    public Integer getQuantity(){
        return this.quantity;
    }

}
