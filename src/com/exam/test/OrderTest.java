package com.exam.test;


import com.exam.models.Item;
import com.exam.models.Order;
import com.exam.models.OrderItem;
import junit.framework.Assert;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class OrderTest {

    /**
     * There was an issue testing this kind of pattern for the taxOfOneHundred method
     * get Order total is not testable without going to create an order
     */
    @Test
    void taxOfOnehundred() throws IOException, ClassNotFoundException {
        OrderItem or = new OrderItem();
        Assert.assertEquals(8.333, 8.333 , or.getOrderTotal(100, 12));
    }

    /**
     * create order test
     *
     */

    @Test
    void testCreateOrder() throws IOException, ClassNotFoundException {
        List<OrderItem> orders = new ArrayList<>();
        OrderItem or = new OrderItem();

        Item item1 = new Item(1, "test1", 1, 2);
        orders.add(new OrderItem( item1, 1));

        or.createOrder(orders);
        Assert.assertSame(String.valueOf(item1), orders, orders);
    }

}